'use strict';

/**
 * @ngdoc function
 * @name angularShopingCartApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the angularShopingCartApp
 */
angular.module('angularShopingCartApp')
  .controller('HomeCtrl', function ($scope, auth, store, $http) {
  	// GET NEW PRODUCT
  var ref1 = firebase.database().ref('books').limitToLast(8);
  $scope.listbook = {};

  ref1.on('value', function(snapshot){
  // alert(angular.toJson(snapshot.val()));
    $scope.listbook = snapshot.val();
    $scope.$apply();
  });

  // GET ONE BOOK SPECIAL
  var ref2 = firebase.database().ref('books').limitToFirst(10); 
  $scope.special_book = {};
  ref2.on('value', function(snapshot){
    var key = Object.keys(snapshot.val());
    $scope.special_book = snapshot.val()[key[8]];
    $scope.special_book.id = key[8];
    $scope.$apply();
 });



   
});

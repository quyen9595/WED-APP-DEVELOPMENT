'use strict';

/**
 * @ngdoc function
 * @name angularShopingCartApp.controller:SiteCtrl
 * @description
 * # SiteCtrl
 * Controller of the angularShopingCartApp
 */
angular.module('angularShopingCartApp')
  .controller('SiteCtrl', function ($scope, auth, store, addtoCart) {

    // LOGOUT FUNCTION
    $scope.auth = auth;
    $scope.logout = function() {
      var key = store.get('userCartKey');
      var refDel = firebase.database().ref().child('carts/' + key);
      refDel.remove();
      store.remove('userCartKey');

      auth.signout();
      store.remove('profile');
      store.remove('token');
      auth.isAuthenticated = false;
    }

    $scope.service = addtoCart;
    	// COUNT ITEM IN CART
    // identify this is your cart. The first time, userCartKey is null, so we check change 
    //  firebase cart and update userCartKey
    //  second solution is every use visit page will be set key. And delete if loggout
    //  or so long for return

    if(store.get('userCartKey')){
      var newCartKey = store.get('userCartKey');
    }else{
      var newCartKey = firebase.database().ref().child('carts').push().key;
        store.set('userCartKey', newCartKey);
    }

    $scope.bookInCart = {};
    var refCart = firebase.database().ref('carts/' + newCartKey);
    refCart.on('value', function(snapshot){
      if(newCartKey == null){$scope.countCart = 0 }
      $scope.bookInCart = snapshot.val();
      if($scope.bookInCart == null) { $scope.countCart = 0} else{
        $scope.countCart = Object.keys($scope.bookInCart).length;
      }
      $scope.$apply();
  });

    $scope.addtocart = function(e){
     var bookid = $(e.target).data('id');
      addtoCart.addtocart(bookid);
      }
  });
